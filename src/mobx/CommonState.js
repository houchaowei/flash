import {observable, action} from "mobx";

class CommonState {
    @observable name = '小红';
    
    @action setName (name) {
        this.name = name;
    }
}

export default CommonState
