export const mockAgentList = {
    "code": 200,
    "msg": "",
    "values": {
        "list": [
            {
                "id": "A169380E2200E12A19B853A8320EE64A",
                "package_name": "初级套餐",
                "subtitle": "完美的初级套餐",
                "tags": "热卖",
                "ori_price": 29900,
                "price": 19900,
                "img": "[\"20190301\\/e67f559bad97888f348466245dd75617.jpg\"]",
                "platform_type": 4
            },
            {
                "id": "A169380F57CB418EC16B35D6047C2709",
                "package_name": "中级套餐",
                "subtitle": "美丽的中级套餐",
                "tags": "推荐",
                "ori_price": 59900,
                "price": 39900,
                "img": "[\"20190301\\/a18fb43413c012537115ba48d22e9a86.jpg\"]",
                "platform_type": 2
            },
            {
                "id": "A169380F68CFF092B27DE77AB8E328F7",
                "package_name": "中级套餐",
                "subtitle": "美丽的中级套餐",
                "tags": "推荐",
                "ori_price": 59900,
                "price": 39900,
                "img": "[\"20190301\\/a18fb43413c012537115ba48d22e9a86.jpg\"]",
                "platform_type": 2
            }
        ],
        "page": 1
    }
}